f2/router
=========

Provides routing for requests to your application. 


Services added to F2::container():

* `F2\Router\Router`


Easy Access Functions added:

* `F2::router()` returns the Router instance
* `F2::serve()` will dispatch requests to the router


Serving Requests
----------------

```php
require('../vendor/autoload.php');

F2::router()->map('GET', '/', function(): Psr\Http\Message\ResponseInterface {
    return F2::createResponse()->withBody(F2::createStream("Hello World!"));
});

F2::serve();
```

