<?php
declare(strict_types=1);

namespace F2\Router;

use F2;
use F2\Router\Contracts\StrategyInterface;
use F2\Router\Contracts\ServerResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use function is_callable, is_array, is_scalar, json_encode;

class DefaultStrategy implements StrategyInterface {
    /**
     * {@inheritdoc}
     */
    public function process($handler, array $vars, ServerRequestInterface $request, ServerResponseInterface $response): void {
        if (is_callable($handler)) {
            $this->processCallableHandler( $handler, $vars, $request, $response );
        } else {
            throw new LogicException("Handler must be callable");
        }
    }

    protected function processCallableHandler( callable $handler, array $vars, ServerRequestInterface $request, ServerResponseInterface $response ) {
        $ref = new \ReflectionFunction( $handler );
        $handler = F2::container()->autowire( $handler, $vars, [ $request, $response ] );

        $result = $handler();

        if ($response->writable()) {
            $this->translateResult($response, $result);
        }
    }

    protected function translateResult(ServerResponseInterface $response, $result) {
        $contentType = $response->getHeader('Content-Type')[0] ?? null;
        if ($contentType !== null) {
            $contentType = explode("/", strtolower($contentType));
        }

        if ($contentType === null && (
            is_array($result) ||
            is_scalar($result) ||
            $result === null ||
            $result instanceof \JsonSerializable
        )) {
            // We'll declare this as application/json
            $response->writeHead(200, ['Content-Type' => 'application/json']);
            $response->end(json_encode($result));
            return;
        }

        if ($result instanceof \Generator) {
            F2::defer($this->makeGeneratorWrapper($result, $response, $contentType));
            return;
        }

        if ($contentType === null) {
            // Default content type when casting
            $response->writeHead(200, ['Content-Type' => 'text/html; charset=utf-8']);
        }

        $response->end(''.$result);
    }

    protected function makeGeneratorWrapper(\Generator $generator, ServerResponseInterface $response, array $contentType=null) {
        return function() use ($generator, $response, $contentType) {
            // Be consistent with stuff emitted without generator
            $ct = $contentType ? implode("/", $contentType) : null;

            $ended = false;

            while ($generator->valid()) {
                $current = $generator->current();
                if (is_resource($current) || method_exists($current, 'then')) {
                    $temp = yield $current;
                    if ($temp instanceof \Throwable) {
                        $generator->throw($temp);
                    } else {
                        $generator->send($temp);
                    }
                } elseif ($ended) {
                    throw new RuntimeException("Can't emit data in chunks with this content type");
                } else {
                    if ($ct === null) {
                        $response->writeHead(200, ['Content-Type' => 'application/json']);
                        $response->end(json_encode($current));
                        $ended = true;
                    } elseif ($ct === 'application/json') {
                        $response->end(json_encode($current));
                        $ended = true;
                    } else {
                        $response->write(''.$current);
                    }
                    $generator->send(yield $current);
                }
            }
            $ret = $generator->getReturn();
            if ($ret !== null) {
                if (!$ended) {
                    $this->translateResult($response, $ret);
                } else {
                    throw new RuntimeException("Can't output return value when response has ended becuase of yielded output");
                }
            }
        };
    }

}
