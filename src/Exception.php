<?php
namespace F2\Router;

class Exception extends \F2\Common\Exception implements Contracts\ExceptionInterface {
    use HttpExceptionTrait;
}
