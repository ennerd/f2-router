<?php
namespace F2\Router;

use F2;
use F2\Common\SingletonTrait;

/**
 * Implements the helper methods from Contracts\RouteCollectorInterface by using the
 * $this->addRoute() method.
 */
trait CollectorTrait {

    public function get(string $path, $handler): void {
        $this->addRoute('GET', $path, $handler);
    }

    public function post(string $path, $handler): void {
        $this->addRoute('POST', $path, $handler);
    }

    public function put(string $path, $handler): void {
        $this->addRoute('PUT', $path, $handler);
    }

    public function patch(string $path, $handler): void {
        $this->addRoute('PATCH', $path, $handler);
    }

    public function delete(string $path, $handler): void {
        $this->addRoute('DELETE', $path, $handler);
    }

    public function head(string $path, $handler): void {
        $this->addRoute('HEAD', $path, $handler);
    }

    public function options(string $path, $handler): void {
        $this->addRoute('OPTIONS', $path, $handler);
    }

}

