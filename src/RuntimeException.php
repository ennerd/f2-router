<?php
namespace F2\Router;

class RuntimeException extends LogicException implements Contracts\ExceptionInterface {
    use HttpExceptionTrait;
}
