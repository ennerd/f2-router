<?php
namespace F2\Router;

class LogicException extends \F2\Common\LogicException implements Contracts\ExceptionInterface {
    use HttpExceptionTrait;
}
