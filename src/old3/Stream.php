<?php
declare(strict_types=1);

namespace F2\Router;

use Psr\Http\Message\StreamInterface;

/**
 * Implementation of PSR HTTP streams that acts like a fifo buffer by using
 * a stream pair. One for reading and one for writing.
 */
class Stream implements StreamInterface {

    protected $pair;
    protected $closed = false;

    public function __construct() {
        $this->pair = stream_socket_pair(STREAM_PF_UNIX, STREAM_SOCK_STREAM, STREAM_IPPROTO_IP);
    }

    /**
     * {@inheritdoc}
     */
    public function __toString() : string
    {
        if (!$closed) {
            throw new LogicException("Can't cast the stream to a string, unless you close it first.");
        }
        return stream_get_contents($this->pair[1]);
    }

    /**
     * {@inheritdoc}
     */
    public function close() : void
    {
        if ($this->closed) {
            return;
        }
        $this->closed = true;
        fclose($this->pair[0]);
        $this->pair[0] = null;
    }

    /**
     * {@inheritdoc}
     *
     * The specification does not support stream pairs, so we can't return the resource as is. Instead 
     * we close the write pipe, and return the read pipe.
     */
    public function detach()
    {
        if ($this->closed) {
            return null;
        }
        $this->close();
        $result = $this->pair[1];
        $this->pair[1] = null;
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getSize() : ?int
    {
        if (!$this->closed || !$this->pair[1]) {
            return null;
        }

        $stats = fstat($this->pair[1]);
        if ($stats !== false) {
            return $stats['size'];
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function tell() : int
    {
        throw new \RuntimeException("Append only stream don't tell");
    }

    /**
     * {@inheritdoc}
     */
    public function eof() : bool
    {
        if (!$this->closed || !$this->pair[1]) {
            return false;
        }

        return feof($this->pair[1]);
    }

    /**
     * {@inheritdoc}
     */
    public function isSeekable() : bool
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function seek($offset, $whence = SEEK_SET) : void
    {
        throw new \RuntimeException("An output buffer is not seekable");
    }

    /**
     * {@inheritdoc}
     */
    public function rewind() : void
    {
        $this->seek(0);
    }

    /**
     * {@inheritdoc}
     */
    public function isWritable() : bool
    {
        if ($this->closed) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function write($string) : int
    {
        if (!$this->pair[0] || $this->closed) {
            throw new \RuntimeException("Can't write to closed stream");
        }

        $result = fwrite($this->pair[0], $string);

        if (false === $result) {
            throw new \RuntimeException("Could not write to stream");
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function isReadable() : bool
    {
        if (!$this->pair[1]) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function read($length) : string
    {
        if (!$this->isReadable()) {
            throw new \RuntimeException("Stream isn't readable");
        }

        if (feof($this->pair[1])) {
            return '';
        }

        $result = fread($this->pair[1], $length);

        if (false === $result) {
            throw new \RuntimeException("Could not read from stream");
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getContents() : string
    {
        if (!$this->isReadable()) {
            throw new \RuntimeException("Stream isn't readable");
        }

        if ($this->isWritable()) {
            throw new \RuntimeException("Stream is still writable, so we can't give you all the contents.");
        }

        $result = stream_get_contents($this->resource);
        if (false === $result) {
            throw new \RuntimeException("Could not read from stream");
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getMetadata($key = null)
    {
        if ($key === null) {
            return ['no-metadata' => 'will-be-provided'];
        } else {
            return null;
        }
    }
}
