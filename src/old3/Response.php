<?php
declare(strict_types=1);

namespace F2\Router;

class Response extends AbstractResponse {

    protected $body;

    public function __construct(StreamInterface $body) {
        $this->body = $body;
    }

    public function getBody(): StreamInterface {
        return $this->body;
    }

    public function withBody(StreamInterface $body): StreamInterface {
        if ($body === $this->body) {
            return $this;
        }

        $new = clone $this;
        $new->body = $body;
        return $new;
    }
}
