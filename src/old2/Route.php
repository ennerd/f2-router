<?php
namespace F2\Router;

class Route implements Contracts\RouteInterface {
    public function __construct(string $method, string $path, $handler) {
        $this->_method = $method;
        $this->_path = $path;
        $this->_handler = $handler;
    }

    public function getMethod(): string {
        return $this->_method;
    }

    public function getPath(): string {
        return $this->_path;
    }

    public function getCallable(): callable {
        // @TODO: Handler should be wrapped up so that it does not need any arguments
        return $this->_handler;
    }
}

