<?php declare(strict_types=1);

namespace F2\Router;

use F2;
use Psr\Http\Message\{ResponseInterface, ServerRequestInterface};
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ExceptionHandlerMiddleware implements ExceptionHandlerMiddlewareInterface {

    public function __construct(\Throwable $throwable) {
        $this->throwable = $throwable;
    }

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
        return F2::createResponse()->withBody(F2::createStream("Unhandled exception ".$e->getMessage()." in ExceptionHandlerMiddleware"));
    }
}
