<?php declare(strict_types=1);

namespace F2\Router;

use F2;
use Psr\Http\Message\{ResponseInterface, ServerRequestInterface};
use Psr\Http\Server\MiddlewareInterface;

interface ExceptionHandlerMiddlewareInterface extends MiddlewareInterface {}
