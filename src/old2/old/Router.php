<?php
namespace F2\Router;

use F2;
use F2\Common\SingletonTrait;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


use nikic fastroute
class Router implements Contracts\RouterInterface {
    use RouterTrait;

    // Route to invoke if we resolve to here
    protected $_handler;
    // Contains exact path components
    protected $_parts = [];
    // Contains variable path components
    protected $_vars = [];

    public static function getInstance(): Contracts\RouterInterface  {
        static $instance;

        if (!$instance) {
            $instance = new static();
        }

        return $instance;
    }

    public function map(string $method, string $path, $handler): Contracts\RouteInterface {
        if (!isset($this->routes[$method])) {
            $this->routes[$method] = [];
        }

        if ($path[0] !== '/') {
            throw new InvalidPathException($path);
        }

        $route = new Route($method, $path, $handler);

        $parts = explode("/", $method.'/'.ltrim($path, "/"));

        $this->insert($parts, $route);

        return $route;
    }

    public function setHandler(Route $handler): void {
        if ($this->_handler) {
            throw new DuplicateRouteException($handler->getPath());
        }
        $this->_handler = $handler;
    }

    protected function insert(array $path, Route $route): void {
        $component = array_shift($path);

        if ($component === null) {
            $this->setHandler($route);
            return;
        }

        $next = null;

        if ($next === null) {
            // Assume it is a constant path component
            if (!isset($this->_parts[$component])) {
                $this->_parts[$component] = $next = new static();
            }
        }

        $next->insert($path, $route);

        var_dump($this->_parts);
    }
}
