<?php
declare(strict_types=1);

namespace F2\Router;

class InvalidPathException extends Exception implements Contracts\RouterExceptionInterface {
    public function __construct(string $path) {
        parent::__construct("Invalid path '$path' when adding to router.");
    }
}

