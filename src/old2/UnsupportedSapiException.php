<?php
namespace F2\Router;

class UnsupportedSapiException extends \F2\Common\Exception {
    public function __construct(string $sapi) {
        parent::__construct("Unsupported SAPI '$sapi'");
    }
}
