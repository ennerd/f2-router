<?php
declare(strict_types=1);

namespace F2\Router\Server;

abstract class AbstractProtocolHandler {
    abstract public static function detect(string $initialBytes): bool;

    public function __construct(Client $client) {
        $this->client = $client;
    }

    /**
     * When new data is available, it is sent here
     */
    abstract public function data(string $data);

    protected function connected(): bool {
        return !!$this->client;
    }

    /**
     * Write something to the client
     */
    protected function write(string $data) {
        if (!$this->client) {
            throw new IOException("Client not connected trying to write");
        }
        $this->client->write($data);
    }

    protected function close() {
        if (!$this->client) {
            throw new IOException("Client not connected");
        }
        $this->client->close();
        $this->client = null;
    }
}
