<?php
namespace F2\Router\Server;

use F2\Router\RuntimeException;

class IOException extends RuntimeException {}
