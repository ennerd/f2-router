<?php
namespace F2\Router\Server;

use F2\Router\HttpExceptionTrait;
use F2\Router\Contracts\ExceptionInterface;

class ProtocolException extends \F2\Common\Exception implements ExceptionInterface {
    use HttpExceptionTrait;

    protected $statusCode = 400;
    protected $reasonPhrase = "Bad Request";
}
