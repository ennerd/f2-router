<?php
declare(strict_types=1);

namespace F2\Router\Server;

use F2;
use F2\Common\Event;
use F2\Common\EventEmitterTrait;
use F2\Common\Helpers\LoggingMethodsTrait;
use F2\Router\Contracts\ServerInterface;
use F2\Router\Contracts\ServerClientInterface;
use F2\Router\LogicException;

class Client implements ServerClientInterface {
    use EventEmitterTrait;
    use LoggingMethodsTrait;

    const CLIENT_DISCONNECT_EVENT = self::class.'::DISCONNECT_EVENT';
    const DESTROY_EVENT = self::class.'::DESTROY_EVENT';

    protected $server;
    protected $socket;
    protected $readBuffer = null;
    protected $protocolHandler = null;

    public function __construct(ServerInterface $server, $socket) {
        $this->server = $server;
        $this->socket = $socket;
        $this->handleConnection();
    }

    public function prependReadBuffer(string $chunk): void {
        // Consider checking if socket exists. If implemented, then read() should also check before outputting the buffer.
        if ($this->readBuffer === null) {
            $this->readBuffer = $chunk;
        } else {
            $this->readBuffer = $chunk.$this->readBuffer;
        }
    }

    public function read(int $length=8192): ?string {
        // Output any prepended data first
        if ($this->readBuffer !== null) {
            if (strlen($this->readBuffer) > $length) {
                $result = substr($this->readBuffer, 0, $length);
                $this->readBuffer = substr($this->readBuffer, $length);
                return $result;
            }
            $result = $this->readBuffer;
            $this->readBuffer = null;
            return $result;
        }
        if (!$this->socket) {
            throw new LogicException("Trying to read on a disconnected socket");
        }
        if (feof($this->socket)) {
            $this->_handleClientDisconnect();
            return null;
        }
        $result = fread($this->socket, $length);
        if ($result === false) {
            $this->_handleSocketFailure();
        } else {
            return $result;
        }
    }

    public function write(string $chunk): bool {
        $this->assertConnected();
        fwrite($this->socket, $chunk);
        return true;
    }

    public function close(): void {
        $this->assertConnected();
        fclose($this->socket);
        $this->socket = null;
        return;
    }

    protected function handleConnection() {
        return F2::defer(function() {
            $infiniteLoopCount = 0;
            while ($this->socket) {
                if ($this->readBuffer === null) {
                    yield F2::readable($this->socket);
                }
                $data = $this->read();
                if ($data === null) {
                    if ($infiniteLoopCount++ === 100) {
                        $this->debug("handleConnection: Infinite Loop detector triggered");
                        $this->close();
                    }
                    continue;
                }
                $infiniteLoopCount = 0;
                if ($this->protocolHandler) {
                    \call_user_func([$this->protocolHandler, 'data'], $data);
                } else {
                    $this->detectProtocol($data);
                }
            }
            //$this->debug('handleConnection: finished');
        });
    }

    protected function detectProtocol($data): void {
        $protocolHandlers = [HttpProtocolHandler::class];
        foreach ($protocolHandlers as $handler) {
            if ($handler::detect($data)) {
                $this->prependReadBuffer($data);
                $this->protocolHandler = new $handler($this);
                return;
            }
        }
        $this->notice("Unsupported protocol (bytes={initialBytes}...)", [ 'initialBytes' => substr($data, 0, 20) ]);
        $this->write("HTTP/1.0 505 HTTP Version Not Supported\r\n\r\nNot supported\r\n");
        $this->close();
    }

    protected function assertConnected(): void {
        if ($this->socket === null) {
            throw new IOException("Client not connected");
        }
    }

    protected function _handleClientDisconnect(): void {
        $this->socket = null;
        $this->emit(new Event(static::CLIENT_DISCONNECT_EVENT, [ 'src' => $this ]));
    }

    protected function _handleSocketFailure(): void {
        // Appears to be disconnected
        $error = sprintf("Socket failure: '%s'", socket_last_error($this->socket));
        $this->socket = null;
        throw new IOException($error);
    }

    public function __destruct() {
        $this->server->emitClientDisconnectEvent($this);
        $this->emit(new Event(static::DESTROY_EVENT, [ 'src' => $this ]));
        if ($this->socket) {
            fclose($this->socket);
        }
    }
}
