<?php
declare(strict_types=1);

namespace F2\Router\Server;

use F2\Common\Helpers\LoggingMethodsTrait;

class HttpProtocolHandler extends AbstractProtocolHandler {
    use LoggingMethodsTrait;

    const READY_PHASE = 'READY';
    const REQUEST_LINE_PHASE = 'REQUEST_LINE';
    const HEADERS_PHASE = 'HEADERS';
    const REQUEST_CHUNKED_BODY_PHASE = 'CHUNKED_REQUEST_BODY';
    const REQUEST_BODY_PHASE = 'REQUEST_BODY';
    const RESPONSE_PHASE = 'RESPONSE';

    public static function detect(string $bytes): bool {
        $nlPos = strpos($bytes, "\n");
        if ($nlPos > 4096) {
            // Probably not an HTTP request
            return false;
        }
        $line = rtrim(substr($bytes, 0, $nlPos));
        $parts = explode(" ", $line);
        if (sizeof($parts) < 3) {
            return false;
        }
        $protocol = array_pop($parts);
        if ($protocol !== 'HTTP/1.1' && $protocol !== 'HTTP/1.0') {
            return false;
        }
        return true;
    }

    protected $phase = self::READY_PHASE;
    protected $buffer = '';
    protected $currentRequest = null;

    public function data(string $chunk) {
        try {
            $m = microtime(true);
            // Phases: 0=request method 1=headers, 2=request body, 3=done
            $this->buffer .= $chunk;
            $bufferLength = strlen($this->buffer);

            while ($this->phase !== static::RESPONSE_PHASE && false !== ($next = strpos($this->buffer, "\r\n"))) {
                $line = substr($this->buffer, 0, $next);
                $this->buffer = substr($this->buffer, $next + 2);
                $bufferLength -= $next + 2;

                if ($this->phase === static::READY_PHASE || $this->phase === static::REQUEST_LINE_PHASE) {
                    //$this->debug("REQUEST LINE: {line}", ['line' => $line]);
                    $line = trim($line);
                    if ($line === '') {
                        //$this->debug("- ignoring empty line");
                        // Ignore initial empty lines: https://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html#sec4.1
                        continue;
                    }
                    $parts = explode(" ", $line);
                    if (sizeof($parts) !== 3) {
                        throw new ProtocolException("HTTP-request malformed");
                    }
                    $this->currentRequest = [
                        'REQUEST' => [
                            'RAW' => $line,
                            'PROTOCOL' => array_pop($parts),
                            'URI' => array_pop($parts),
                            'METHOD' => array_pop($parts),
                        ]
                    ];
                    $this->phase = static::HEADERS_PHASE;
                } elseif ($this->phase === static::HEADERS_PHASE) {
                    //$this->debug("HEADERS LINE: {line}", ['line' => $line]);
                    if ($line === '') {
                        //$this->debug('Headers complete');

                        if (isset($this->currentRequest['HEADERS']['transfer-encoding']) && strtolower($this->currentRequest['HEADERS']['transfer-encoding'][0])==='chunked') {
                            $this->phase = static::REQUEST_CHUNKED_BODY_PHASE;
                        } elseif (isset($this->currentRequest['HEADERS']['content-length'])) {
                            $this->phase = static::REQUEST_BODY_PHASE;
                        } else {
                            $this->phase = static::RESPONSE_PHASE;
                        }
                    } else {
                        $split = strpos($line, ":");
                        if ($split === false) {
                            //$this->debug('Invalid header {line}', ['line' => $line]);
                            throw new ProtocolException("Invalid header");
                        }
                        $name = strtolower(substr($line, 0, $split));
                        $value = ltrim(substr($line, $split+1));
                        //$this->debug("Header {name}: {value}", ['name' => $name, 'value' => $value]);
                        $this->currentRequest['HEADERS'][$name][] = ltrim($value);
                    }
                } elseif ($this->phase === static::REQUEST_BODY_PHASE) {
                    //$this->debug("REQUEST BODY LINE: {line}", ['line' => $line]);
                    if (isset($this->currentRequest['HEADERS']['content-length'])) {
                        $contentLength = intval($this->currentRequest['HEADERS']['content-length'][0]);
                        //$this->debug('Expecting body because of Content-Length: '.$contentLength);
                        if ($bufferLength >= $contentLength) {
                            $this->currentRequest['BODY']= substr($this->buffer, 0, $contentLength);
                            $this->buffer = substr($this->buffer, $contentLength);
                            $bufferLength -= $contentLength;
                            //$this->debug('Request body complete');
                            $this->phase = static::RESPONSE_PHASE;
                        } else {
                            //$this->debug('Partial body');
                            return;
                        }
                    } else {
                        throw new ProtocolException("Expecting Content-Length header");
                        $this->currentRequest['BODY'] = null;
                        $this->phase = static::RESPONSE_PHASE;
                        break;
                    }
                } elseif ($this->phase === static::REQUEST_CHUNKED_BODY_PHASE) {
                    die("CHUNKY");
                } elseif ($this->phase === static::RESPONSE_PHASE) {
                    //$this->debug("RESPONSE LINE: {line}", ['line' => $line]);
                    throw new ProtocolException("Received content while in the response phase");
                }
            }
            if ($this->phase === static::RESPONSE_PHASE) {
                $this->write("HTTP/1.0 200 Ok\r\n\r\nHello!");
                $this->close();
            } else {
                throw new ProtocolException("Wrong phase");
            }
        } catch (\F2\Router\Contracts\ExceptionInterface $e) {
            $this->debug(get_class($e).': '.$e->getMessage());
            $this->write("HTTP/1.0 ".$e->getStatusCode()." ".$e->getReasonPhrase()."\r\n\r\n".$e->getReasonPhrase());
            $this->close();
        } catch (\Throwable $e) {
            $this->debug(get_class($e).': '.$e->getMessage());
            $this->write("HTTP/1.0 500 ".$e->getMessage()."\r\n\r\n".$e->getMessage());
            $this->close();
        }
    }
}
