<?php
declare(strict_types=1);

namespace F2\Router;

use F2;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Dispatcher implements Contracts\DispatcherInterface {
    public function handle(ServerRequestInterface $request): ResponseInterface {
    }
}
