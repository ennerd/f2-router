<?php
declare(strict_types=1);

namespace F2\Router;

use F2;
use F2\Common\Event;
use F2\Common\EventEmitterTrait;
use F2\Common\Helpers\LoggingMethodsTrait;
use F2\Router\Contracts\ServerInterface;
use F2\Router\Contracts\ServerClientInterface;
use F2\Router\Contracts\ServerResponseInterface;
use F2\Router\Server\Client;
use Psr\Http\Message\ServerRequestInterface;
use Laminas\Diactoros\ServerRequestFactory;

class Server implements ServerInterface {
    use EventEmitterTrait;
    use LoggingMethodsTrait;

    /**
     * {@inheritdoc}
     */
    public static function serve(array $options=[]): void {
        $server = F2::container()->get(ServerInterface::class);

        $server->run($options);
    }

    public function __construct(Contracts\StrategyInterface $strategy) {
        $this->strategy = $strategy;
    }

    protected $strategy;
    protected $serverSocket;
    protected $running = true;
    protected $connections;

    public function run(array $options=[]): void {
        try {
            if (PHP_SAPI !== 'cli') {
                $client = null;
                $this->emitClientConnectEvent($client);
                $serverRequest = ServerRequestFactory::fromGlobals();
                $response = new ServerResponse();
                $server->handle($serverRequest, $response);
                $disconnectedChecker = function() use ($client, &$disconnectedChecker) {
                    if (\connection_status() !== 0) {
                        $this->emit(new Event(static::CLIENT_DISCONNECT_EVENT, [ 'src' => $this, $client => $client ]));
                    } else {
                        F2::defer($disconnectedChecker);
                    }
                };
                F2::defer($disconnectedChecker);
            } else {
                $this->runHttpServer($options);
            }
        } catch (\Throwable $e) {
            echo "Fatal error: ".$e->getMessage()."\n".$e->getTraceAsString();
        }
    }


    public function emitClientConnectEvent(?ServerClientInterface $client): void {
        //$this->debug("Client connected");
        $this->emit(new Event(static::CLIENT_CONNECT_EVENT, [ 'src' => $this, 'client' => $client ]));
    }

    public function emitClientDisconnectEvent(?ServerClientInterface $client): void {
        //$this->debug("Client disconnected");
        $this->emit(new Event(static::CLIENT_DISCONNECT_EVENT, [ 'src' => $this, 'client' => $client ]));
    }

    protected function runHttpServer(array $options=[]): void {
        $options = $options + [
            'local_socket' => 'tcp://0.0.0.0:8080',
            ];

        $this->serverSocket = \stream_socket_server($options['local_socket'], $errno, $errstr);

        $this->waitForConnections();
    }

    protected function waitForConnections() {
        return F2::defer(function() {
            while ($this->serverSocket && $this->running) {
                yield F2\readable($this->serverSocket);
                $socket = stream_socket_accept($this->serverSocket);
                $client = new Client($this, $socket);
                $this->emitClientConnectEvent($client);
                // Don't keep reference to client. They live their own life until garbage collected.
                // We'll see it again when it calls $this->notifyClientDisconnect()
                $client = null;
            }
        });
    }


    /**
     * {@inheritdoc}
     */
    public function handle(ServerRequestInterface $request, ServerResponseInterface $response): void {
        try {
            $route = F2::router()->resolve($request->getMethod(), $request->getRequestTarget());
            if ($route === null) {
                throw new NotFoundException("No route to '".$request->getRequestTarget()."'");
            }
        } catch (\Throwable $e) {
            $route = F2::router()->throw($request->getMethod(), $request->getRequestTarget(), $e);
        }

        $this->applyStrategy( $route[1], $route[2], $request, $response );
    }

    public function applyStrategy( $handler, array $vars, ServerRequestInterface $request, ServerResponseInterface $response) {
        $this->strategy->process($handler, $vars, $request, $response);
    }

}
