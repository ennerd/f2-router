<?php
declare(strict_types=1);

namespace F2\Router;

use F2\Router\Contracts\ServerResponseInterface;
use F2\Router\Streams\WritableStreamTrait;

use function header, headers_sent, header_remove, headers_list,
    remove_header, strtolower, strlen, ltrim, substr, strpos,
    is_array, sprintf, http_response_code;

/**
 * Wraps the standard PHP output buffer
 */
class ServerResponse implements Contracts\ServerResponseInterface {
    use WritableStreamTrait;

    const DEFAULT_STATUS_CODE = 200;

    /**
     * Respect the WritableStreamTrait wishes
     */
    protected $closed = false;
    protected $writer = null;
    protected function _write(string $chunk): bool {
        return call_user_func($this->writer, $chunk);
    }
    protected function _close() {
        $this->closed = true;
    }
    public function writable(): bool {
        return !$this->closed;
    }
    public function closed(): bool {
        return $this->closed;
    }

    protected $statusMessage, $implicitStatusMessage;
    protected $generateDate = true;

    public function __construct() {
        if (PHP_SAPI === 'cli') {
            throw new \RuntimeException("ServerResponse can't be used from the command line");
        }
        if (headers_sent()) {
            throw new \RuntimeException("Headers already sent, so ServerResponse won't work.");
        }
        $this->writer = function($content) {
            echo $content;
            return true;
        };
    }

    protected function assertHeadersNotSent(): void {
        if ($this->headersSent()) {
            throw new RuntimeException("Headers already sent");
        }
    }

    public function getHeader(string $name): array {
        $result = [];
        $headers = headers_list();
        $name = strtolower($name).':';
        $l = strlen($name);
        foreach ($headers as $header) {
            if (strtolower(substr($header, 0, $l+1)) === $name) {
                $result[] = ltrim(substr($header, $l+1));
            }
        }
        return $result;
    }

    public function getHeaderNames(): iterable {
        $headers = headers_list();
        foreach ($headers as $header) {
            yield substr($header, 0, strpos($header, ":"));
        }
    }

    public function getHeaders(): iterable {
        $result = [];
        foreach (headers_list() as $header) {
            $l = strpos($header, ":");
            $name = strtolower(substr($header, 0, $l+1));
            $value = ltrim(substr($header, $l+1));
            if (!isset($result[$name])) {
                $result[$name] = $value;
            } else {
                if (!is_array( $result[$name] )) {
                    $result[$name] = [ $result[$name], $value ];
                } else {
                    $result[$name][] = $value;
                }
            }
        }
        return $result;
    }

    public function hasHeader(string $name): bool {
        $name = strtolower($name);
        $headers = $this->getHeaders();
        foreach ($headers as $headerName => $value) {
            if ($headerName === $name) {
                return true;
            }
        }
    }

    public function headersSent(): bool {
        return headers_sent();
    }

    public function removeHeader(string $name) {
        $this->assertHeadersNotSent();
        remove_header($name);
    }

    public function generateDate(bool $set=null): bool {
        if ($set !== null) {
            $this->generateDate = $set;
        }
        return $this->generateDate;
    }

    public function setHeader(string $name, $value) {
        $this->assertHeadersNotSent();
        if (!is_array($value)) {
            header(sprintf('%s: %s', $name, $value), true);
        } else {
            $index = 0;
            foreach ($value as $headerValue) {
                header(sprintf('%s: %s', $name, $headerValue), $index++ === 0);
            }
        }
    }

    public function statusCode(int $set=null): int {
        if ($set !== null) {
            $this->assertHeadersNotSent();
            if ($this->statusMessage === null) {
                $statusMessage = $this->implicitStatusMessage = ServerResponseInterface::PHRASES[static::DEFAULT_STATUS_CODE];
            } else {
                $statusMessage = $this->statusMessage;
            }
            header($_SERVER['SERVER_PROTOCOL'].' '.$this->statusCode.' '.$statusMessage, true);
        }
        // Find the default status code
        return http_response_code();
    }

    public function statusMessage(string $set=null): ?string {
        if ($set !== null) {
            $this->assertHeadersNotSent();
            $this->statusMessage = $set;
            header($_SERVER['SERVER_PROTOCOL'].' '.$this->statusCode().' '.$this->statusMessage, true);
            return $set;
        }
        if ($this->statusMessage !== null) {
            return $this->statusMessage;
        }
        if ($this->implicitStatusMessage !== null) {
            return $this->implicitStatusMessage;
        }
        return null;
    }

    public function writeHead(int $statusCode, $statusMessage=null, iterable $headers=null): ServerResponseInterface {

        $this->assertHeadersNotSent();

        if ($headers !== null) {
            if ($statusMessage !== null && !is_string($statusMessage)) {
                throw new \InvalidArgumentException('$statusMessage must be a string');
            }
        } elseif ($statusMessage !== null) {
            if (is_array($statusMessage)) {
                $headers = $statusMessage;
                $statusMessage = null;
            } elseif (!is_string($statusMessage)) {
                throw new \InvalidArgumentException('$statusMessage must be a string or an array to be used as $headers');
            }
        }

        if ($statusMessage !== null) {
            $this->statusMessage($statusMessage);
        }

        if ($headers !== null) {
            foreach ($headers as $name => $value) {
                if (is_string($value)) {
                    header(sprintf('%s: %s', $name, $value), false);
                } elseif (is_iterable($value)) {
                    foreach ($value as $singleValue) {
                        header(sprintf('%s: %s', $name, $value), false);
                    }
                }
            }
        }
        return $this;
    }

    // null = indetermined, false = no, true = yes
    protected $useChunkedTransfer = null;

    public function requestChunkedTransfer(): bool {
        if ($this->useChunkedTransfer !== null) {
            return $this->usedChunkedTransfer;
        }
        // If there is a content length header, we assume we can't chunk
        $contentLength = $this->getHeader('Content-Length');
        if (isset($contentLength[0])) {
            return $this->useChunkedTransfer = false;
        }
        // If transfer encoding is set to something other than chunked, we can't chunk
        $transferEncoding = $this->getHeader('Transfer-Encoding');
        if (isset($transferEncoding[0])) {
            if (strtolower($transferEncoding[0])!=='chunked') {
                return $this->useChunkedTransfer = false;
            } else {
                return $this->useChunkedTransfer = true;
            }
        }
        if ($this->headersSent()) {
            return $this->useChunkedTransfer = false;
        }
        if (isset($_SERVER["SERVER_SOFTWARE"]) && substr($_SERVER["SERVER_SOFTWARE"], 0, 6)==='nginx/') {
            $this->setHeader('X-Accel-Buffering', 'no');
            $oldWriter = $this->writer;
            $this->writer = function($content) use ($oldWriter) {
                $result = $oldWriter($content);
                while (ob_get_length() > 0) ob_end_flush();
                flush();
                return true;
            };
        } else {
            throw new \Exception("requestChunkedTransfer has not been implemented for others than nginx yet...");
        }
        return $this->useChunkedTransfer = true;
    }
}
