<?php
declare(strict_types=1);
namespace F2\Router;

use F2;
use F2\Common\SingletonTrait;
use F2\Common\Event;
use FastRoute\RouteCollector;
use FastRoute\RouteParser\Std as RouteParser;
use FastRoute\DataGenerator\GroupCountBased as GroupCountDataGenerator;
use FastRoute\Dispatcher\GroupCountBased as GroupCountDispatcher;

class Collector implements Contracts\CollectorInterface {
    use CollectorTrait;

    /**
     * Registering routes:
     *
     * F2::events()->on(Collector::SETUP_ROUTES_EVENT, function($e) {
     *     $e->collector->get('/some-route/', $callable);
     * });
     */
    const SETUP_ROUTES_EVENT = self::class.'::SETUP_ROUTES_EVENT';

    protected $routeCollector;

    public function __construct(array $options=[]) {
        // Defaults
        $options += [
            'routeParser' => RouteParser::class,
            'dataGenerator' => GroupCountDataGenerator::class,
            'dispatcher' => GroupCountDispatcher::class,
        ];
        $this->routeCollector = new RouteCollector(
            new $options['routeParser'](),
            new $options['dataGenerator']()
        );

// should move this, need to be lazy
        F2::events()->emit(new Event(static::SETUP_ROUTES_EVENT, [
            'src' => $this,
        ]));
    }

    public function addRoute(string $method, string $path, $handler): void {
        $this->routeCollector->addRoute($method, $path, $handler);
    }

    public function addGroup(string $prefix, callable $callback): void {
        $this->routeCollector->addGroup($method, $path, $handler);
    }

    public function getData(): array {
        return $this->routeCollector->getData();
    }

}
