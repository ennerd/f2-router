<?php
namespace F2\Router;

trait HttpExceptionTrait {
    /**
     * Declare these properties if you wish to provide more sane status codes
     */
    // protected $statusCode;
    // protected $reasonPhrase;

    /**
     * {@inheritdoc}
     */
    public function getStatusCode() {
        return property_exists($this, 'statusCode') ? $this->statusCode : 500;
    }

    /**
     * {@inheritdoc}
     */
    public function getReasonPhrase() {
        return property_exists($this, 'reasonPhrase') ? $this->reasonPhrase : "Internal Server Error";
    }
}
