<?php
declare(strict_types=1);
namespace F2\Router;

use F2;
use F2\Common\SingletonTrait;
use F2\Common\Event;
use FastRoute\RouteCollector;
use FastRoute\RouteParser\Std as RouteParser;
use FastRoute\DataGenerator\GroupCountBased as GroupCountDataGenerator;
use FastRoute\Dispatcher\GroupCountBased as GroupCountDispatcher;
use FastRoute\Dispatcher;

class Router extends Collector implements Contracts\RouterInterface {
    protected function getDispatcher(): Dispatcher {
        static $dispatcher;
        if (!$dispatcher) {
            // @TODO: Cache $this->getData() so we don't have to do so much heavy lifting every time
            $dispatcher = new GroupCountDispatcher($this->getData());
        }
        return $dispatcher;
    }

    public function resolve(string $httpMethod, string $target): ?array {
        $result = $this->getDispatcher()->dispatch($httpMethod, $target);
        if ($result[0] === 0) {
            return null;
        }
        return $result;
    }

    protected $exceptionHandlers = [];

    /**
     * Catch exceptions
     */
    public function catch(string $className, $handler, array $vars=[]): void {
        $this->exceptionHandlers[$className] = [ \FastRoute\Dispatcher::FOUND, $handler, $vars ];
    }

    /**
     * Resolve exceptions
     */
    public function throw(string $httpMethod, string $target, \Throwable $throwable): ?array {
        $queue = [ get_class($throwable) ];
        while (isset($queue[0])) {
            $next = array_shift($queue);
            if (isset($this->exceptionHandlers[$next])) {
                return $this->exceptionHandlers[$next];
            }
            foreach (class_implements($next) as $interface) {
                $queue[] = $interface;
            }
            if ($parent = get_parent_class($next)) {
                $queue[] = $parent;
            }
        }
        throw new RouteNotResolvedException($httpMethod, $target);
    }
}
