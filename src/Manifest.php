<?php
namespace F2\Router;

use Psr\Http\Message\StreamFactoryInterface;

class Manifest extends \F2\Common\AbstractManifest {

    public function getDependencies(): iterable {
        return [
            \F2\Common\Manifest::class,
            \F2\Container\Manifest::class,
            \F2\Http\Manifest::class,
            \F2\Promise\Manifest::class,
        ];
    }

    /**
     * These are essential methods that should be added to the F2 class
     */
    public function getF2Methods(): iterable {
        // router() is assumed to be more intuitive than collector()
        yield "router"                  => Contracts\RouterInterface::class;
        yield "serve"                   => [ Server::class, 'serve' ];
    }

    public function registerConfigDefaults(array $config): array {
/*
        // We're replacing the StreamFactory from the f2/http-package
        foreach ($config['container/services'] as $k => &$service) {
            if ($service[2] === StreamFactoryInterface::class) {
                $service[1] = StreamFactory::class;
            }
        }
*/
        $config['container/services'][] = [ 'class', Router::class, Contracts\RouterInterface::class, true ];
        $config['container/services'][] = [ 'class', Collector::class, Contracts\CollectorInterface::class, true ];
        $config['container/services'][] = [ 'class', DefaultStrategy::class, Contracts\StrategyInterface::class, true ];
        $config['container/services'][] = [ 'class', Server::class, Contracts\ServerInterface::class, true, 'arguments' => [ Contracts\StrategyInterface::class ] ];
        return $config;
    }

}
