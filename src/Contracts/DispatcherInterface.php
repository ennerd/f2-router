<?php
declare(strict_types=1);

namespace F2\Router\Contracts;
use Psr\Http\Server\RequestHandlerInterface;

interface DispatcherInterface extends RequestHandlerInterface {
}
