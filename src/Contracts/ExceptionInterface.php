<?php
namespace F2\Router\Contracts;

interface ExceptionInterface {

    public function getStatusCode();
    public function getReasonPhrase();

}
