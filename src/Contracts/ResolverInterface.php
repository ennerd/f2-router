<?php
declare(strict_types=1);
namespace F2\Router\Contracts;

interface ResolverInterface {
    public function resolve(string $httpMethod, string $uri): ?array;
}
