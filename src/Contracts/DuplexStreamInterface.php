<?php
namespace F2\Router\Stream;

/**
 * Modelled after node.js because PSR-7 has a really bad API that is virtually unusable in async environments.
 */
interface DuplexStreamInterface extends ReadableStreamInterface, WritableStreamInterface  {
}

