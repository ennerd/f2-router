<?php
namespace F2\Router\Contracts;

use F2\Common\Contracts\EventEmitterInterface;

interface ServerClientInterface extends EventEmitterInterface {
    public function __construct(ServerInterface $server, $socket);
}
