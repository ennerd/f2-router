<?php
namespace F2\Router\Contracts;

use Psr\Http\Message\ServerRequestInterface;

interface ServerInterface {

    const CLIENT_CONNECT_EVENT = self::class.'::CLIENT_CONNECT_EVENT';
    const CLIENT_DISCONNECT_EVENT = self::class.'::CLIENT_DISCONNECT_EVENT';

    /**
     * Handle requests that come in. If SAPI is cli then act as a server
     */
    public static function serve(): void;

    /**
     * Process a single request and that's it.
     */
    public function handle(ServerRequestInterface $request, ServerResponseInterface $response): void;
}
