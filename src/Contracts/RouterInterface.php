<?php
declare(strict_types=1);
namespace F2\Router\Contracts;

interface RouterInterface extends CollectorInterface, ResolverInterface {
}
