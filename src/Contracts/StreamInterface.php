<?php
namespace F2\Router\Contracts;

use F2\Common\Contracts\EventEmitterInterface;
/**
 * Modelled after node.js because PSR-7 has a really bad API that is virtually unusable in async environments.
 */
interface StreamInterface extends EventEmitterInterface {
}
