<?php
namespace F2\Router\Contracts;

/**
 * Modelled after node.js because PSR-7 has a really bad API that is virtually unusable in async environments.
 */
interface WritableStreamInterface extends StreamInterface  {
    // https://nodejs.org/api/stream.html#stream_writable_streams

    /**
     * The 'close' event is emitted when the stream and any of its underlying resources (a file descriptor,
     * for example) have been closed. The event indicates that no more events will be emitted, and no
     * further computation will occur.
     */
    const CLOSE_EVENT = self::class.'::CLOSE_EVENT';

    /**
     * False if the stream is closed or if it due to any reason can't be written to temporarily or permanently
     */

    public function writable(): bool;
    /**
     * True if the stream is closed permanently
     */
    public function closed(): bool;

    // Write some bytes to output
    public function write(string $chunk): bool;

    // Write the chunk and emit self::CLOSE_EVENT
    public function end(string $chunk=null): StreamInterface;
}
