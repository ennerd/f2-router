<?php
declare(strict_types=1);
namespace F2\Router\Contracts;

interface CollectorInterface {
    public function addRoute(string $method, string $path, $handler): void;
    public function addGroup(string $prefix, callable $callback): void;

    public function get(string $path, $handler): void;
    public function post(string $path, $handler): void;
    public function put(string $path, $handler): void;
    public function patch(string $path, $handler): void;
    public function delete(string $path, $handler): void;
    public function head(string $path, $handler): void;
    public function options(string $path, $handler): void;

    // @TODO: This interface is not optimal. It is very nikic/fastroute specific.
    public function getData(): array;
}
