<?php
namespace F2\Router\Contracts;

/**
 * Modelled after node.js because PSR-7 has a really bad API that is virtually unusable in async environments.
 */
interface ReadableStreamInterface extends StreamInterface  {
    // https://nodejs.org/api/stream.html#stream_readable_streams

    const CLOSE_EVENT = "ReadableStreamInterface::CLOSE_EVENT";

    public function read(int $size=null);
    public function unshift(string $chunk);
    public function readable(): bool;
}
