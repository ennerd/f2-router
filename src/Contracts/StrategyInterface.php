<?php
declare(strict_types=1);

namespace F2\Router\Contracts;

use Psr\Http\Message\ServerRequestInterface;
use F2\Router\Contracts\ServerResponseInterface;

interface StrategyInterface {

    /**
     * Process a handler and return a ResponseInterface.
     *
     * @param mixed $handler                   Handler provided when routes were registered
     * @param array $vars                      Variables extracted from the route
     * @param ServerRequestInterface $request  The request we're processing
     */
    public function process($handler, array $vars, ServerRequestInterface $request, ServerResponseInterface $response): void;

}
