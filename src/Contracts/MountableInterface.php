<?php
namespace F2\Router\Contracts;

/**
 * Classes that implement this interface can be mounted in the router by
 * implementing the onMount method.
 */
interface MountableInterface {
    public function getRoutes(RouteCollector $collector): void;
}
