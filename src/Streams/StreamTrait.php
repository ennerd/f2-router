<?php
declare(strict_types=1);

namespace F2\Router\Streams;

use F2\Common\EventEmitterTrait;

trait StreamTrait {
    use EventEmitterTrait;
}
