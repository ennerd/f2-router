<?php
declare(strict_types=1);

namespace F2\Router\Streams;

use F2\Common\Event;
use F2\Router\Contracts\WritableStreamInterface;
use F2\Router\RuntimeException;

/**
 * Expects a `protected method _send(string $bytes): void` to be implemented
 * on the hosting class
 */
trait WritableStreamTrait {
    use StreamTrait;

    /**
     * Methods expected to be overridden
     */
    protected function _write(string $chunk): bool {
        throw new LogicException("Implement the '".self::class."::_write()' method");
    }
    public function writable(): bool {
        throw new LogicException("Implement the '".self::class."::writable()' method");
    }

    protected function _close(string $chunk) {
        throw new LogicException("Implement the '".self::class."::_close()' method");
    }
    public function closed(): bool {
        throw new LogicException("Implement the '".self::class."::closed()' method");
    }

    /**
     * @see WritableStreamInterface::write()
     */
    public function write(string $chunk): bool {
        if ($this->closed()) throw new RuntimeException("Stream is closed");
        if (!$this->writable()) throw new RuntimeException("Stream is not writable");
        return $this->_write($chunk);
    }

    /**
     * @see WritableStreamInterface::end()
     */
    public function end(string $chunk=null): WritableStreamInterface {
        if ($this->closed()) throw new RuntimeException("Stream is closed");
        if ($chunk !== null) {
            if (!$this->writable()) throw new RuntimeException("Stream is not writable");
            $this->write($chunk);
        }
        $this->emit(new Event(WritableStreamInterface::CLOSE_EVENT, ['src' => $this]));
        $this->_close();
        return $this;
    }
}
